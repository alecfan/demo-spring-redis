package cn.eastseven.redis;

import java.io.Serializable;

public class User implements Serializable {

	@Override
	public String toString() {
		return "User [uid=" + uid + ", address=" + address + "]";
	}

	private static final long serialVersionUID = -5867092720940199037L;

	private String uid;

	private String address;

	public String getUid() {
		return uid;
	}

	public void setUid(String uid) {
		this.uid = uid;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

}
