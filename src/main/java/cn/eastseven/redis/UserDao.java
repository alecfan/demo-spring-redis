package cn.eastseven.redis;

public interface UserDao {
	/**
	 * @param uid
	 * @param address
	 */
	void save(final User user);

	/**
	 * @param uid
	 * @return
	 */
	User read(String uid);

	/**
	 * @param uid
	 */
	void delete(String uid);
}
