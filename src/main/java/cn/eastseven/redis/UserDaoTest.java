package cn.eastseven.redis;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class UserDaoTest {
	
	final Logger log = LoggerFactory.getLogger(getClass());
	
	private ApplicationContext app;
	private UserDao userDao;

	@Before
	public void before() throws Exception {
		app = new ClassPathXmlApplicationContext("classpath:spring/application-config.xml");
		userDao = (UserDao) app.getBean("userDao");
	}

	@Test
	public void crud() {
		// -------------- Create ---------------
		String uid = "u123456";
		String address1 = "上海";
		User user = new User();
		user.setAddress(address1);
		user.setUid(uid);
		userDao.save(user);
		
		log.debug("create: "+user.toString());
		
		// ---------------Read ---------------
		user = userDao.read(uid);

		Assert.assertEquals(address1, user.getAddress());
		log.debug("read: "+user.toString());
		
		// --------------Update ------------
		String address2 = "北京";
		user.setAddress(address2);
		userDao.save(user);

		user = userDao.read(uid);

		Assert.assertEquals(address2, user.getAddress());
		log.debug("update: "+user.toString());
		
		// --------------Delete ------------
		userDao.delete(uid);
		user = userDao.read(uid);
		Assert.assertNull(user);
		log.debug("delete");
	}
}
